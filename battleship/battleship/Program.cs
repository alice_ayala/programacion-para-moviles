﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace battleship
{
    class Program
    {
        static void Main(string[] args)
        {
            string jugador_1, jugador_2;
            int b1, b2;//numero de barcos 
            int Tarreglo;//tamaño del arreglo en el tablero 
            int a1, a2;// ataque 
            

            Console.WriteLine("Jugador 1 ingrese su nombre");
            jugador_1 = Console.ReadLine();
            Console.WriteLine("Jugador 2 ingrese su nombre");
            jugador_2 = Console.ReadLine();


            Console.WriteLine("Ingrese el numero de casillas que desea tener en su tablero");
            Tarreglo = int.Parse(Console.ReadLine());

            Console.WriteLine("Jugador 1 ingrese el numero de barcos que desea tener");
            b1 = int.Parse(Console.ReadLine());

            while (b1 >= Tarreglo)
            {
                Console.WriteLine("Por favor ingrese un numero menor al de las casillas del tablero ");
                b1 = int.Parse(Console.ReadLine());

            }


            Console.WriteLine("Jugador 2 ingrese el numero de barcos que desea tener");
            b2 = int.Parse(Console.ReadLine());


            while (b2 >= Tarreglo)
            {
                Console.WriteLine("Por favor ingrese un numero menor al de las casillas");
                b2 = int.Parse(Console.ReadLine());
            }
            Console.ReadLine();

            string lugar;
            int i;
            int[] tablero1;
            int[] tablero2;
            tablero2 = new int[Tarreglo];
            tablero1 = new int[Tarreglo];

            for (i = 0; i <= b1; i++)
            {
                for (int j = 0; j < Tarreglo; j++)
                {
                    Console.WriteLine("jugador 1 " + (i + 1) + " en que posicion desea colocar su barco " + (j + 1) + "?");
                    Console.WriteLine("presione d si es lo quiere colocar en esa posicon");
                    Console.WriteLine("En caso contrario tecle k");

                    lugar = Console.ReadLine();
                    if (lugar == "d")
                    {
                        if (tablero1[i] == 1)
                        {
                            Console.WriteLine("Ese lugar ya esta ocupado");
                        }
                        else
                        {
                            tablero1[i] = 1;
                            Console.Clear();
                            break;
                        }

                    }
                }
            }

            for (i = 0; i <= b2; i++)
            {
                for (int j = 0; j < Tarreglo; j++)
                {
                    Console.WriteLine("Su barco " + (i + 1) + " en que posicion desea colocarlo " + (j + 1) + "?");
                    Console.WriteLine("presione d si es lo quiere colocar en esa posicon");
                    Console.WriteLine("En caso contrario tecle k");

                    lugar = Console.ReadLine();
                    if (lugar == "d")
                    {
                        if (tablero2[i] == 1)
                        {
                            Console.WriteLine("Ese lugar ya esta ocupado");
                        }
                        else
                        {
                            tablero2[i] = 1;
                            Console.Clear();
                            break;
                        }

                    }
                }
            
                do
                {
                    Console.WriteLine("turno del jugador 1 " + jugador_1 + "");
                    Console.WriteLine("ingrese la coordenada que desea atacar");
                    a1 = int.Parse(Console.ReadLine());
                    if (tablero2[a1] == 1)
                    {
                        tablero2[a1] = 0;
                        Console.WriteLine("su barco se ha hundido");
                        b2 = b2 - 1;
                    }

                    Console.WriteLine("turno del jugador 2 " + jugador_1 + "");
                    Console.WriteLine("ingrese la coordenada que desea atacar");
                    a2 = int.Parse(Console.ReadLine());
                    if (tablero1[a2] == 1)
                    {
                        tablero1[a2] = 0;
                        Console.WriteLine("su barco se ha hundido");
                        b1 = b1 - 1;
                    }
                } while (b1 == 0 || b2 == 0);

                if (b1 < b2)
                {
                    Console.WriteLine("jugador 1 " + jugador_1+"ha ganado");
                    Console.WriteLine("tiene" + b1 + "barcos ");

                }
                else
                {
                    Console.WriteLine("jugador 2 " + jugador_2 + "ha ganado");
                    Console.WriteLine("tiene" + b2 + "barcos ");
                }

            }
        }
    }
}

